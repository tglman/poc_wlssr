use std::sync::{Arc, Mutex};
use serde::Serialize;
use wlssr::{AppConfig, Controller, Endpoint, Method, RealEndpoint, TemplateResponseBuilder};

#[derive(Clone, Serialize, Debug)]
struct Wine {
    amount: u32,
}

#[derive( Default)]
struct WineController {
    data: Mutex<Vec<Wine>>,
}
impl WineController {
    fn list(&self) -> Vec<Wine> {
        let data = self.data.lock().unwrap();
        data.clone()
    }

    fn add(&self, amount: u32) -> Vec<Wine> {
        let mut data = self.data.lock().unwrap();
        data.push(Wine { amount });
        data.clone()
    }
}

#[actix_web::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut app = AppConfig::new();
    app.add_controller(WineController::default());
    app.run().await?;
    Ok(())
}




/////////// TO GENERATE WITH MACROS
impl Controller for WineController {
    fn get_endpoints(self: Arc<Self>) -> Vec<Arc<dyn Endpoint>> {
        let mut endpoins = Vec::<Arc<dyn Endpoint>>::new();
        let endpoint = Arc::new(RealEndpoint::new(
            Method::GET,
            "list.html".to_owned(),
            Box::new(|c: &Self, _r| {
                let data = c.list();
                Box::new(TemplateResponseBuilder::new("list".to_owned(),Some(data)))
            }),
            self.clone(),
        ));
        endpoins.push(endpoint);
        let endpoint = Arc::new(RealEndpoint::new(
            Method::GET,
            "add".to_owned(),
            Box::new(|c: &Self, r| {
                let data = c.add(r.get_parameter("amount").parse().unwrap());
                Box::new(TemplateResponseBuilder::new("list".to_owned(), Some(data)))
            }),
            self.clone(),
        ));
        endpoins.push(endpoint);
        endpoins
    }
}


