pub use actix_web::http::Method;
use actix_web::{web, web::Query, Error as ActixError, HttpRequest, HttpResponse, HttpServer};
use std::{collections::HashMap, sync::Arc};
use thiserror::Error;
use serde::Serialize;
use handlebars::Handlebars;

#[derive(Clone)]
pub struct AppConfig {
    host: String,
    port: u16,
    controllers: Vec<Arc<dyn Controller>>,
}
unsafe impl Send for AppConfig {}
impl AppConfig {
    pub fn new() -> Self {
        Self {
            host: "localhost".to_owned(),
            port: 8080,
            controllers: Vec::new(),
        }
    }
}

pub trait Endpoint {
    fn invoke(&self, data: &dyn RequestData) -> Box<dyn ResponseBuilder>;
    fn get_method(&self) -> Method;
    fn get_path(&self) -> String;
}

pub trait RequestData {
    fn get_parameter(&self, name: &str) -> String;
}
impl RequestData for HttpRequest {
    fn get_parameter(&self, name: &str) -> String {
        Query::<HashMap<String, String>>::from_query(&self.query_string())
            .unwrap()
            .get(name)
            .unwrap()
            .clone()
    }
}

pub trait ResponseBuilder {
    fn into_response(&self) -> HttpResponse;
}

pub struct TemplateResponseBuilder<T> {
    name:String,
    data:Option<T>,
}
impl<T> TemplateResponseBuilder<T> {
    pub fn new(name:String, data:Option<T>) -> Self {
        Self {name, data}
    }
}
#[derive(Serialize)]
struct Data<T:Serialize> {
    data:T
}

impl<T:Serialize> ResponseBuilder for TemplateResponseBuilder<T> {
    fn into_response(&self) -> HttpResponse {
        let mut reg = Handlebars::new();
        reg.register_template_file(&self.name, format!("./static/view/{}.hbs",self.name)).unwrap();
        let result = if let Some(data) = &self.data { 
            reg.render(&self.name, &Data { data})
        } else {
            reg.render(&self.name, &() )
        }.unwrap();
        HttpResponse::Ok().content_type("text/html").body(result)
    }
}

pub struct RealEndpoint<T> {
    method: Method,
    path: String,
    action: Box<dyn Fn(&T, &dyn RequestData) -> Box<dyn ResponseBuilder>>,
    controller: Arc<T>,
}
impl<T> RealEndpoint<T> {
    pub fn new(
        method: Method,
        path: String,
        action: Box<dyn Fn(&T, &dyn RequestData) -> Box<dyn ResponseBuilder>>,
        controller: Arc<T>,
    ) -> Self {
        Self {
            method,
            path,
            action,
            controller,
        }
    }
}

impl<T> Endpoint for RealEndpoint<T> {
    fn invoke(&self, data: &dyn RequestData) -> Box<dyn ResponseBuilder> {
        (self.action)(&self.controller, data)
    }
    fn get_path(&self) -> String {
        self.path.clone()
    }
    fn get_method(&self) -> Method {
        self.method.clone()
    }
}

pub trait Controller {
    fn get_endpoints(self: Arc<Self>) -> Vec<Arc<dyn Endpoint>>;
}

#[derive(Debug, Error)]
pub enum Errors {
    #[error("Actix Error {0}")]
    Actix(#[from] ActixError),
    #[error("Io Error {0}")]
    Io(#[from] std::io::Error),
}

pub type WR<X> = Result<X, Errors>;

impl AppConfig {
    pub fn add_controller(&mut self, controller: impl Controller + 'static) {
        self.controllers.push(Arc::new(controller));
    }
    pub fn set_listening_host(&mut self, host: String) {
        self.host = host;
    }
    pub fn set_listening_port(&mut self, port: u16) {
        self.port = port;
    }

    pub async fn run(self) -> WR<()> {
        HttpServer::new(move || {
            let mut app = actix_web::App::new();
            for controller in self.clone().controllers {
                let to_register = controller.get_endpoints();
                for singe in to_register {
                    app = app.route(
                        &singe.get_path(),
                        web::method(singe.get_method())
                            .to(move |req: HttpRequest| singe.invoke(&req).into_response()),
                    )
                }
            }
            app
        })
        .bind("127.0.0.1:8080")?
        .run()
        .await?;
        Ok(())
    }
}
